<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Usuarios extends CI_Controller {

	public function index()
	{
        if ($this->session->userdata('login')) //aqui pregunta si ya hay una cesion iniciada o abierta, SE escribe session seguido por delante y por detras <-session->
        {
            redirect ('usuarios/panel','refresh'); //si ya esta abierto, entonces lo redirecciona a usuarios
        }
        else//si no, le muestra un form para llenar
        {
            $this->load->view('head');
		    $this->load->view('loginform'); 
		    $this->load->view('footer');
        }
		
    }

    public function validarusuario()
    {
        //AQUI RECEPCIONA LOS DATOS INGRESADOS POR EL USUARIO
        $login=$_POST['login'];
        $password=md5($_POST['password']); //AQUI BORRAR MD5, PORQ EN EL FRONT NO ESTA CIFRADO
        //AQUI SE HACE LA CONSULTA DE VALIDAR, SI EL USUARIO YA ESTA REGISTRADO, DEVUELVE UN REGISTRO, SINO UN FORMULARIO VACIO
        $consulta=$this->usuarios_model->validar($login, $password);
        //aqui preg, que si la consulta esta llenado con algun dato, por eso es mayor a cero
        if($consulta->num_rows()>0)
        {
            //aqui dice q si SI es asi, entonces mestra un registro de login y tipo
            foreach ($consulta->result() as $row)
            {
                $this->session->set_userdata('login',$row->login); //aqui muestra los registros de la base de datos del front, como login y tipo
                $this->session->set_userdata('tipo',$row->tipo);
                redirect('usuarios/panel','refresh');
            }            
        }
        //sino redirecciona al formulario de login vacio, para llenar
        else
        {
            redirect('usuarios/index','refresh');
        }
    }

    public function panel ()
    {
        if ($this->session->userdata('login')) //si ya hay una cesion abierta
        {
            $this->load->view('head');
		    $this->load->view('panelvista'); 
		    $this->load->view('footer');
        }
        else//si no, le muestra un form para llenar
        {
            redirect('usuarios/index','refresh');
        }
    }

    public function logout()
    {
        $this->session->sess_destroy();
        redirect('usuarios/index','refresh');
    }
}