<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Paises extends CI_Controller {

	public function index()
	{
		$this->load->view('head');
		$data['paises']=$this->paises_model->retornarPaises();
		$this->load->view('listapaises',$data);
		$this->load->view('footer');
	}

	public function modificar()
	{
		$idpais=$_POST['idpais'];
		$data['pais']=$this->paises_model->recuperarPais($idpais);
		$data['capital']=$this->paises_model->recuperarPais($idpais);
		$data['clima']=$this->paises_model->recuperarPais($idpais);
		$data['poblacion']=$this->paises_model->recuperarPais($idpais);
		$data['hombres']=$this->paises_model->recuperarPais($idpais);
		$data['mujeres']=$this->paises_model->recuperarPais($idpais);
		$this->load->view('head');
		$this->load->view('modificarpaisform',$data);
		$this->load->view('footer');
	}

	public function modificardb()
	{
		$idpais=$_POST['idpais'];
		$pais=$_POST['pais'];
		$capital=$_POST['capital'];
		$clima=$_POST['clima'];
		$poblacion=$_POST['poblacion'];
		$hombres=$_POST['hombres'];
		$mujeres=$_POST['mujeres'];
		$data['pais']=$pais;
		$data['capital']=$capital;
		$data['clima']=$clima;
		$data['poblacion']=$poblacion;
		$data['hombres']=$hombres;
		$data['mujeres']=$mujeres;
		$this->paises_model->modificarPais($idpais,$data);
		//ESTE ES METODO CORTO
		redirect('paises/index','refresh'); //aqui se da dos parametros, a donde quiere ir y refrescar
		
		/* ESTE ES EL METODO LARGO DE ACTUALIZACION
		$data['paises']=$this->paises_model->retornarPaises();//al modelo donde ira, es decir, aqui digo q desp de modificar ira  directamente a la vista principal
		$this->load->view('head');
		$this->load->view('listapaises',$data);//AQUI SE ANOTA A DONDE QUIERO IR DESPUES DE MODIFICAR
		$this->load->view('footer');*/
	}

	public function agregar()
	{
		$this->load->view('head');
		$this->load->view('agregarform');
		$this->load->view('footer');
	}

	public function agregardb()
	{
		$pais=$_POST['pais'];
		$capital=$_POST['capital'];
		$clima=$_POST['clima'];
		$poblacion=$_POST['poblacion'];
		$hombres=$_POST['hombres'];
		$mujeres=$_POST['mujeres'];
		$data['pais']=$pais;
		$data['capital']=$capital;
		$data['clima']=$clima;
		$data['poblacion']=$poblacion;
		$data['hombres']=$hombres;
		$data['mujeres']=$mujeres;
		$this->paises_model->agregarPais($data);
		redirect('paises/index','refresh');//METODO CORTO DE REDIRECCION

		/*METODO LARGO
		$this->load->view('head');
		$this->load->view('agregarmensaje',$data);
		$this->load->view('footer');*/
	}

	public function eliminardb()
	{
		$idpais=$_POST['idpais'];
		$pais=$_POST['pais'];
		$data['pais']=$pais;
		$this->paises_model->eliminarPais($idpais);
		redirect('paises/index','refresh');
		/*
		$this->load->view('head');
		$this->load->view('eliminarmensaje',$data);
		$this->load->view('footer');*/
	}
}
